import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TelegramMapComponent } from './telegram-map.component';

describe('TelegramMapComponent', () => {
  let component: TelegramMapComponent;
  let fixture: ComponentFixture<TelegramMapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TelegramMapComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TelegramMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
